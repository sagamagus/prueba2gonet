package com.gonet;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int n1=0;
        do {
            try {
                System.out.println("Ingresa el numero 1");
                n1 = keyboard.nextInt();
            } catch (InputMismatchException ime){
                System.out.println("¡Cuidado! Solo puedes insertar números. ");
                keyboard.next();
            }
        } while (n1==0);
        keyboard.nextLine();
        int n2=0;
        do {
            try {
                System.out.println("Ingresa el numero 2");
                n2 = keyboard.nextInt();
            } catch (InputMismatchException ime){
                System.out.println("¡Cuidado! Solo puedes insertar números. ");
                keyboard.next();
            }
        } while (n2==0);
        keyboard.nextLine();
        String s1, s2;
        System.out.println("Ingresa el primer texto");
        s1 = keyboard.nextLine();
        System.out.println("Ingresa el segundo texto");
        s2 = keyboard.nextLine();
        StringDataType<String> stringDataType = new StringDataType<>();
        stringDataType.performAll(s1,s2);
        NumericDataType<Integer> numericDataType = new NumericDataType<>();
        numericDataType.performAll(n1,n2);
    }
}
