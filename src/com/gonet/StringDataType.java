package com.gonet;

import com.gonet.interfaces.Operations;

public class StringDataType<T> implements Operations {

    @Override
    public void addition(Object a, Object b) {
        System.out.println("The addition of 2 strings is: " + a.toString() + b.toString());
    }

    @Override
    public void subtraction(Object a, Object b) {
        //Log.e("Msg:", "The addition of 2 strings is: " a + b);
        System.out.println("Msg: This operation can't be perform on strings");
    }

    @Override
    public void multiplication(Object a, Object b) {
        System.out.println("Msg: This operation can't be perform on strings");
    }

    @Override
    public void division(Object a, Object b) {
        System.out.println("Msg: This operation can't be perform on strings");
    }

    public void performAll(T a, T b) {
        addition(a,b);
        subtraction(a,b);
        multiplication(a,b);
        division(a,b);
    }
}
