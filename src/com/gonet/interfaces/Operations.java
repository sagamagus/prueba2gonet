package com.gonet.interfaces;

public interface Operations<T> {
    void addition(T a, T b);
    void subtraction(T a, T b);
    void multiplication(T a, T b);
    void division(T a, T b);
}
