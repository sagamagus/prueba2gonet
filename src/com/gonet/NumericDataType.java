package com.gonet;

import com.gonet.interfaces.Operations;

public class NumericDataType<N extends Number> implements Operations<N> {

    Double resultOperationInt;

    public void performAll(N a, N b) {
        addition(a,b);
        subtraction(a,b);
        multiplication(a,b);
        division(a,b);
    }

    @Override
    public void addition(N a, N b) {
        resultOperationInt = a.doubleValue()+b.doubleValue();
        System.out.println("The addition of two numbers is: " + String.format( "%.2f", resultOperationInt ));
    }

    @Override
    public void subtraction(N a, N b) {
        resultOperationInt = a.doubleValue()-b.doubleValue();
        System.out.println("The subtraction of two numbers is: " + String.format( "%.2f", resultOperationInt ));
    }

    @Override
    public void multiplication(N a, N b) {
        resultOperationInt = a.doubleValue()*b.doubleValue();
        System.out.println("The multiplication of two numbers is: " + String.format( "%.2f", resultOperationInt ));
    }

    @Override
    public void division(N a, N b) {
        resultOperationInt = a.doubleValue()/b.doubleValue();
        System.out.println("The division of two numbers is: " + String.format( "%.2f", resultOperationInt ));
    }

}
